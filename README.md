# Craft 3 Boilerplate by wfp

- [Links](#links)
- [Setup](#setup)
- [Development](#development)
- [Admin](#admin)

## Links

- [Craft CMS 3 Boilerplate by wfp – Repository](https://gitlab.com/wanjapflueger/craft-3-boilerplate.git)
- [Server Requirements for Craft CMS 3](https://docs.craftcms.com/v3/requirements.html#required-php-extensions)
- [Tips and Tricks](https://codepad.co/wanjafriedemann/collection/)
- [Useful Plugins (medium.com)](https://medium.com/webdevs/craft-cms-3-plugins-our-favorites-ba726a31164c)
- [Stuff that can be done without a plugin (nystudio107.com)](https://nystudio107.com/blog/cutting-the-cord-removing-plugins)

## Setup

Run `./install.sh`.

## Development

Start Webserver on `./craft/web`. Open `DEFAULT_SITE_URL` from [.env](./craft/.env) in your Browser (you may also define that manually if you skipped that in setup).

```
DEFAULT_SITE_URL="http://localhost"
```

### Compiling CSS and JS (including Watch)

```
cd ./craft/templates
npm start
```

## Admin

The admin area can be reached over http://example.test/_admin_.

### Password reset for user

In table `users` find user and add the following password hash (password: `NewPassword`):

```
$2y$13$ftVWpipDuTRbbqYU1lKNv./Zp7MJekPUezmH5qpH2AUonTQvmWiea
```
