#!/bin/bash

echo

DIR="craft"
if [ -d "$DIR" ]; then
  echo "📝 Setup started"
  echo

  # Install Craft
  ./craft/craft setup

  # Install Plugins
  ./install/plugins.sh

  # Setup Static Files
  ./install/static.sh

  # Setup File Injections
  ./install/injections.sh

  # Setup Template
  ./install/template.sh

  # Architect
  ./install/architect.sh
  
  # Ask for Clean Up after Setup
  echo 
  echo -n "🤨  Would you like to clean up and remove installation files (y/n)? "
  read answerCleanup
  if [ "$answerCleanup" != "${answerCleanup#[Yy]}" ] ;then
    echo
    echo "🧹 Cleaning up ..."

    rm -rf .git
    rm -rf .gitignore
    rm -rf install
    rm -rf install.sh
  else
    echo
    echo "❗️Please remove installation files after setup is completed."
  fi

  # Finish Setup
  echo 
  echo "🚀 Craft 3 CMS Setup completed successfully 🥳"
  echo
else
  # Craft not found, installing for the first time ...

  echo "💾 Installing Craft 3 ..."
  echo

  # Require Craft
  composer create-project craftcms/craft

  # Finish Setup
  echo "📦 Craft was delivered successfully. Run ./install.sh again to begin Setup."
  echo
fi