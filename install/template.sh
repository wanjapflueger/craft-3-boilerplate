#!/bin/bash

echo

DIR="craft"
if [ -d "$DIR" ]; then
  echo "🎨 Template Setup"
  echo

  # remove default template files
  rm -rf craft/templates/

  # move blueprint files
  cp -r install/templates craft

  # install node_modules
  cd craft/templates
  npm i

  # run build
  npm run build
  echo
  echo "💡 Compile CSS/JS:"
  echo "cd craft/templates"
  echo "npm start"

  # Finish Setup
  echo
  echo "🎨 Template Setup completed successfully"
  echo
else
  echo "❌ Could not find Craft"
  echo
fi