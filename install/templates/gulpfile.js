// npm i -D autoprefixer babelify gulp-uglify gulp-browserify gulp-clean-css gulp-concat del gulp gulp-postcss gulp-px-to-rem gulp-sass gulp-sourcemaps gulp-rename gulp-savefile

'use strict';

// Import Requirements
const autoprefixer = require('autoprefixer');
const babelify = require('babelify');
const browserify = require('gulp-browserify');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const del = require('del');
const gulp = require('gulp');
const postcss = require('gulp-postcss');
const px2rem = require('gulp-px-to-rem');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const savefile = require('gulp-savefile');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');

// Custom Variables
const src = '.';
const dist = `${src}/../web/static`;
const distAssets = `${dist}/assets`;
const distImages = `${dist}/images`;

// JavaScript
const js = `${src}/javascript`;
const jsInputFilename = 'index.js';
const jsInput = `${js}/${jsInputFilename}`;
const jsOutputFilename = 'index.js';
const jsOutput = `${distAssets}/javascript`;
const jsComponents = `${src}/_components/**/*.js`;
const jsWatchFiles = `${src}/**/*.js`;

// Stylesheets
const baseFontSize = 16; // base font size in pixel
const postCssPlugins = [
  autoprefixer() // Use .browserslistrc
];
const cssInputFilename = 'index.scss';
const cssInput = `${src}/stylesheets/scss/${cssInputFilename}`;
const cssOutputFilename = 'index.css';
const cssOutput = `${distAssets}/stylesheets`;
const cssWatchFiles = `${src}/**/*.scss`;

/**
 * Sub-Task: Build CSS
 *
 * - Compile SCSS -> CSS
 * - Add Browserprefixes
 * - Minify
 * - Concat to a single File
 * - Create Sourcemaps
 *
 * 1: Fix for Craft 3 Cache not accepting css file via "gulp.dest" -> Rename and write again via "savefile"
 */
gulp.task(
  'build:css',
  () =>
    gulp
      .src(cssInput)
      .pipe(sourcemaps.init()) // init sourcemaps
      .pipe(sass()) // do the sass
      .pipe(postcss(postCssPlugins))
      .pipe(cleanCSS()) // cleanup & minify
      .pipe(px2rem({ rootPX: baseFontSize })) // convert pixels to rem
      .pipe(concat(cssOutputFilename)) // zusammenführen der Dateien (hier nur eine)
      .pipe(sourcemaps.write('.')) // write sourcemaps
      .pipe(gulp.dest(cssOutput)) // output
      .pipe(rename(cssOutputFilename)) // * 1 * //
      .pipe(savefile(cssOutput)) // * 1 * //
);

/**
 * Sub-Task: Build JS
 *
 * - Compile ES6 -> ES5
 * - Create Sourcemaps
 * - Minify (with uglify)
 *
 * 1: Fix for Craft 3 Cache not accepting js file via "gulp.dest" -> Rename and write again via "savefile"
 *
 * TODO fix sourcemaps
 */
gulp.task(
  'build:js',
  () =>
    gulp
      .src([jsInput, jsComponents])
      .pipe(sourcemaps.init({ loadMaps: true })) // init sourcemaps
      .pipe(concat(jsOutputFilename)) // zusammenführen der Dateien (hier nur eine)
      .pipe(
        browserify({
          debug: true,
          transform: [
            babelify.configure({
              presets: ['es2015']
            })
          ]
        })
      )
      .pipe(uglify()) // minify
      .pipe(sourcemaps.write('.')) // write sourcemaps
      .pipe(gulp.dest(`${jsOutput}`)) // output
      .pipe(rename(jsOutputFilename)) // * 1 * //
      .pipe(savefile(`${jsOutput}`)) // * 1 * //
);

/**
 * Task: Watch
 */
gulp.task('watch', function(cb) {
  // Watch SCSS
  gulp.watch(cssWatchFiles, gulp.series('build:css'));

  // Watch JS
  gulp.watch(jsWatchFiles, gulp.series('build:js'));

  cb();
});

/**
 * Task: Clean
 *
 * - Remove stuff
 */
gulp.task('clean', () => {
  return del([`${cssOutput}**/*`, `${jsOutput}**/*`], {
    force: true
  });
});

/**
 * Task: Build
 *
 * The Distribution of this build can be deployed
 *
 * - Start Task "Clean"
 * - Start Sub-Tasks
 */
gulp.task(
  'build',
  gulp.series('clean', gulp.parallel('build:css', 'build:js'))
);

/**
 * Task: Serve
 *
 * Start a local server to serve files from dist/
 */
gulp.task('serve', gulp.series('build', gulp.parallel('watch')));
