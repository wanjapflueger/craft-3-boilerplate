# Craft 3 Template by wfp

**Tip:** Working with Microsofts _Visual Studio Code_? Use this [Extension](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments) to improve readability.

## NPM & Gulp

- Run `npm i` to install dependencies
- Run `npm start` to start watching files with Gulp

## JavaScript

ES6 and jQuery supported.

- [Input](./javascript)
- [Output](./../web/static/assets/javascript)

## SCSS

Use BEM Syntax (read more: http://getbem.com/introduction/)

- [Input](./stylesheets)
- [Output](./../web/static/assets/stylesheets)

## Hooks

_Hooks_ can be set in `app/site/templates/_components/layout/*.twig`. They function like [Twig-Blocks](https://twig.symfony.com/doc/2.x/functions/block.html).

```twig
{# Example for Hook "apfelbaum": #}

{# ? ⚓️ HOOK "apfelbaum" #}
{% set hookApfelbaum %}{% block apfelbaum %}
{% endblock %}
{% endset %}

<div class="container-for-apfelbaum">{{ hookApfelbaum }}</div>
```

**Under certain conditions, hooks can be nested:**

Example: a _hook_ with the label `apfelbaum` can have a preceding _hook_ with the label `beforeApfelbaum` and a following _hook_ with the label `afterApfelbaum`.

A _hook_ can also have children. If these are direct children of `apfelbaum` who follow each other, the labels

- `apfelbaumSection1`,
- `apfelbaumSection2`,
- `apfelbaumSection3`
- etc.

must be used.

**Note:** To add more content to the beginning or end of a _hook_ with predefined content, use the following syntax.

```twig
{% block documentTitle %}Before Content {{ parent() }} After Content{% endblock %}
```

**Semantic structure**

- 📦 `<html>`
- 📦 `<head>`
  - ⚓️ HOOK **meta**
- 📦 `<title>`
  - ⚓️ HOOK **documentTitle**
- 📦 `<body>`
- 📦 `<article class="site">`
  - `<div class="site__header__wrapper>`
    - ⚓️`hook` **beforeHeader** `.site__header__before`
    - ⚓️`hook` **header** `.site__header`
      - ⚓️`hook` **headerSection1** `.site__header__section .site__header__section--1`
      - ⚓️`hook` **headerSection2** `.site__header__section .site__header__section--2`
      - ⚓️`hook` **headerSection3** `.site__header__section .site__header__section--3`
    - ⚓️`hook` **afterHeader** `.site__header__after`
  - `<div class="site__main__wrapper>`
    - ⚓️`hook` **beforeMain** `.site__main__before`
    - ⚓️`hook` **main** `.site__main`
      - `<div class="site__title__wrapper>`
        - ⚓️`hook` **beforeTitle** `.site__title__before`
        - ⚓️`hook` **title** `.site__title`
        - ⚓️`hook` **afterTitle** `.site__title__after`
      - `<div class="site__content__wrapper>`
        - ⚓️`hook` **beforeContent** `.site__content__before`
        - ⚓️`hook` **content** `.site__content`
        - ⚓️`hook` **afterContent** `.site__content__after`
    - ⚓️`hook` **afterMain** `.site__main__after`
  - `<div class="site__footer__wrapper>`
    - ⚓️`hook` **beforeFooter** `.site__footer__before`
    - ⚓️`hook` **footer** `.site__footer`
      - ⚓️`hook` **footerSection1** `.site__footer__section .site__footer__section--1`
      - ⚓️`hook` **footerSection2** `.site__footer__section .site__footer__section--2`
      - ⚓️`hook` **footerSection3** `.site__footer__section .site__footer__section--3`
    - ⚓️`hook` **afterFooter** `.site__footer__after`

**Example Usage for each available Hook:**

```twig
{% block beforeHeader %}beforeHeader{% endblock %}
{% block headerSection1 %}headerSection1{% endblock %}
{% block headerSection2 %}headerSection2{% endblock %}
{% block headerSection3 %}headerSection3{% endblock %}
{% block afterHeader %}afterHeader{% endblock %}
{% block beforeMain %}beforeSiteMain{% endblock %}
{% block beforeTitle %}beforeTitle{% endblock %}
{% block title %}title{% endblock %}
{% block afterTitle %}afterTitle{% endblock %}
{% block beforeContent %}beforeContent{% endblock %}
{% block content %}content{% endblock %}
{% block afterContent %}afterContent{% endblock %}
{% block afterMain %}afterSiteMain{% endblock %}
{% block beforeFooter %}beforeFooter{% endblock %}
{% block footerSection1 %}footerSection1{% endblock %}
{% block footerSection2 %}footerSection2{% endblock %}
{% block footerSection3 %}footerSection3{% endblock %}
{% block afterFooter %}afterFooter{% endblock %}
```

## Entries

### Get Entries

```twig
{% import "_functions/getEntries" as getEntries %}

{% set posts = getEntries.by("category", {id}) %}
{% set posts = getEntries.by("category", {handle}) %}
{% set posts = getEntries.by("section", {handle}) %}
{% set posts = getEntries.by("search", {string}) %}

{% for item in posts %}
  <a href="{{item.url}}">{{ item.title }}</a>
{% endfor %}
```

### Remove Trashed Items

Run the following command in _Craft_ [root direcotry](./../) to remove trashed entries.

```
./craft gc --delete-all-trashed
```
