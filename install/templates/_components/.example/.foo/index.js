var mLogo = {};
(function() {
  const COMPONENT = 'm-logo';
  const SELECTOR = $(`.${COMPONENT}`);
  const DEBUG = false;

  /**
   * Init
   *
   * The JavaScript associated with this Component is only executed if the Component exists in DOM
   */
  this.init = function() {
    if (SELECTOR.length !== 0) {
      mLogo.listen(); // Start listening ...
    } else {
      if (DEBUG) {
        console.log(`Selector not found: '${COMPONENT}'`);
      }
    }
  };

  /**
   * Listen for User Input after Init
   */
  this.listen = function() {
    mLogo.resize(); // Listen for Window Resize
    mLogo.scroll(); // Listen for Window Scroll
  };

  /**
   * Resize
   */
  this.resize = function() {
    window.addEventListener(
      'resize',
      mLogo.debounce(function() {
        if (DEBUG) {
          console.log('resize() after debounce');
        }
      }, 300)
    );

    window.addEventListener(
      'resize',
      mLogo.throttle(function() {
        if (DEBUG) {
          console.log('resize() after throttle');
        }
      }, 300)
    );
  };

  /**
   * Scroll
   *
   * requires throttle() and debounce()
   */
  this.scroll = function() {
    window.addEventListener(
      'scroll',
      mLogo.debounce(function() {
        if (DEBUG) {
          console.log('scroll() after debounce: ' + window.pageYOffset);
        }
      }, 300)
    );

    window.addEventListener(
      'scroll',
      mLogo.throttle(function() {
        if (DEBUG) {
          console.log('scroll() after throttle: ' + window.pageYOffset);
        }
      }, 300)
    );
  };

  /**
   * Throttle (Helper Function)
   * 
   * @param {function} callback Function callback
   * @param {int} limit Toggles every n miliseconds
   */
  this.throttle = function(callback, limit) {
    let wait = false;

    return function() {
      if (!wait) {
        callback.call();
        wait = true;
        setTimeout(function() {
          wait = false;
        }, limit);
      }
    };
  };

  /**
   * Debounce
   *
   * From https://davidwalsh.name/javascript-debounce-function
   * 
   * @param {function} func Function callback
   * @param {int} wait Wait for n miliseconds after end of animation
   */
  this.debounce = function(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this,
        args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };
}.apply(mLogo));

document.addEventListener('DOMContentLoaded', () => {
  mLogo.init();
});
