#!/bin/bash

echo

DIR="craft"
if [ -d "$DIR" ]; then
  echo "📦 Installing plugins ..."
  echo

  cd $DIR

  # Architect
  echo "📦 Architect"
  composer require pennebaker/craft-architect
  ./craft install/plugin architect

  # Clear Cache
  echo "📦 Clear Cache"
  composer require mmikkel/cp-clearcache
  ./craft install/plugin cp-clearcache

  # Child Me
  echo "📦 Child Me"
  composer require mmikkel/child-me
  ./craft install/plugin child-me

  # Dumper
  echo "📦 Dumper"
  composer require studioespresso/craft-dumper
  ./craft install/plugin dumper

  # Element Count
  echo "📦 Element Count"
  composer require aelvan/craft-cp-element-count
  ./craft install/plugin cp-element-count

  # Field Inspect
  echo "📦 Field Inspect"
  composer require mmikkel/cp-field-inspect
  ./craft install/plugin cp-field-inspect

  # HTTP/2 Serverpush
  echo "📦 HTTP/2 Serverpush"
  composer require superbig/craft3-http2serverpush
  ./craft install/plugin http2-server-push

  # Logs
  echo "📦 Logs"
  composer require ether/logs
  ./craft install/plugin logs

  # Plugin Commands
  echo "📦 Plugin Commands"
  composer require ostark/craft-plugin-commands
  echo

  # Redactor
  echo "📦 Redactor (Editor)"
  composer require craftcms/redactor
  ./craft install/plugin redactor

  # Retour
  echo "📦 Retour"
  composer require nystudio107/craft-retour
  ./craft install/plugin retour

  # SEOmatic
  echo "📦 SEOmatic"
  composer require nystudio107/craft-seomatic
  ./craft install/plugin seomatic

  # Super Sort
  echo "📦 Super Sort"
  composer require topshelfcraft/supersort
  ./craft install/plugin supersort

  # Super Table
  echo "📦 Super Table"
  composer require verbb/super-table
  ./craft install/plugin super-table

  # Typogrify
  echo "📦 Typogrify"
  composer require nystudio107/craft-typogrify
  ./craft install/plugin typogrify

  # Twig Perversion
  echo "📦 Twig perversion"
  composer require marionnewlevant/twig-perversion
  ./craft install/plugin twig-perversion

  # Finish Setup
  echo
  echo "📦 Plugins successfully installed and activated"
  echo
  
else

  echo "❌ Could not find Craft"
  echo
fi
