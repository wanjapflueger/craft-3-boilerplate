#!/bin/bash

echo

DIR="craft"
if [ -d "$DIR" ]; then
  echo "💉 Starting Injection ..."
  echo
  
  # .env
  file="craft/.env"
  echo "" >> $file
  echo "# Uploads directory in /web" >> $file
  echo "UPLOADS_PATH=\"assets\"" >> $file
  echo
  echo "🤒 Patient treated: $file"
  tail -1 $file

  # .env.example
  file="craft/.env.example"
  echo "" >> $file
  echo "# Uploads directory in /web" >> $file
  echo "UPLOADS_PATH=\"uploads\"" >> $file
  echo
  echo "🤒 Patient treated: $file"
  tail -1 $file
  
  # .gitignore
  file="craft/.gitignore"
  echo "" >> $file
  echo "web/uploads/*" >> $file
  echo
  echo "🤒 Patient treated: $file"
  tail -1 $file

  # Finish Setup
  echo 
  echo "👩🏼‍⚕️ Injections successfull"
  echo

else

  echo "❌ Could not find Craft"
  echo
fi