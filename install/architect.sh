#!/bin/bash

echo

DIR="craft"
if [ -d "$DIR" ]; then
  echo "📺 Entering the Matrix 💊 ..."
  echo

  # copy architect export file to root folder
  cp ./install/architect.json .
  echo "👨🏼‍🦳 (Architect): Visit /admin/architect/import in Craft and import the contents of ./architect.json"

  # Finish Setup
  echo 
  echo "👨🏼‍✈️ Exiting the Matrix..."
  echo

else

  echo "❌ Could not find Craft"
  echo
fi