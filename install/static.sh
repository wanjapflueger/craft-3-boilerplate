#!/bin/bash

echo

DIR="craft"
if [ -d "$DIR" ]; then
  echo "🏠 Static files ..."
  echo

  echo "🏠 Making directories for statig files ..."

  # copy static dir structure from blueprint
  cp -r install/static $DIR/web

  # create uploads dir
  mkdir $DIR/web/uploads
  touch $DIR/web/uploads/.gitkeep
  
  echo "🌞 Created $DIR/web/static"
  echo "🌞 Created $DIR/web/uploads"

  # Finish Setup
  echo 
  echo "👍 Done with static files"
  echo

else

  echo "❌ Could not find Craft"
  echo
fi